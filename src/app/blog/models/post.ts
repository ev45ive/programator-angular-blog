// ng g i blog/models/post

// dane.json
// => 
// http://www.jsontots.com/

export interface Post {

    id: number;
    userId: number;

    title: string;
    subtitle: string;

    date: string;
    body: string;
}
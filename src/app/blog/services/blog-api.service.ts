import { InjectionToken, Injectable, Inject } from '@angular/core';

export const API_URL = new InjectionToken('Api url token ;-)')

import { HttpClient } from "@angular/common/http"
import { Post } from '../models/post';

@Injectable({
  providedIn: 'root'
})
export class BlogApiService {

  constructor(
    @Inject(API_URL)
    private url: string,
    protected http: HttpClient
  ) { }


  getRecent() {
    return this.http.get<Post[]>(this.url + 'posts', {
      params: {
        _sort: 'date'
      }
    })
  }

  searchPosts(query: string) {
    return this.http.get<Post[]>(this.url + 'posts', {
      params: {
        q: query,
        _sort: 'date'
      }
    })
  }
}

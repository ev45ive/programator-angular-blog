import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecentPostsComponent } from './pages/recent-posts/recent-posts.component';
import { SearchPostsComponent } from './pages/search-posts/search-posts.component';
import { FullPostComponent } from './pages/full-post/full-post.component';


const routes: Routes = [
  {
    path: '', redirectTo: 'posts', pathMatch: 'full'
  },
  {
    path: 'posts', component: RecentPostsComponent
  },
  {
    path: 'search', component: SearchPostsComponent
  }, {
    path: 'posts/:id_post', component: FullPostComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }

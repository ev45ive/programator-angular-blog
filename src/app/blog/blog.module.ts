import { NgModule } from '@angular/core';
import { RecentPostsComponent } from './pages/recent-posts/recent-posts.component';
import { SearchPostsComponent } from './pages/search-posts/search-posts.component';
import { FullPostComponent } from './pages/full-post/full-post.component';
import { PostsListComponent } from './components/posts-list/posts-list.component';
import { PostTeaserComponent } from './components/post-teaser/post-teaser.component';
import { PostSearchBarComponent } from './components/post-search-bar/post-search-bar.component';
import { CommonModule } from '@angular/common';
import { BlogRoutingModule } from './blog-routing.module';
import { environment } from 'src/environments/environment';
import { API_URL, BlogApiService } from './services/blog-api.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    RecentPostsComponent,
    SearchPostsComponent,
    FullPostComponent,
    PostsListComponent,
    PostTeaserComponent,
    PostSearchBarComponent
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: API_URL,
      useValue: environment.api_url
    },
    // {
    //   provide: BlogApiService,
    //   deps: [API_URL/* , DEP1, DEP2, ... */],
    //   useFactory(url/* ,dep1, dep2, ... */) {
    //     return new BlogApiService(url)
    //   },
    // },
    // {
    //   provide: BlogApiService,
    //   useClass: JsonServerBlogApiService,
    //   // deps: [API_URL]
    // },
    // {
    //   provide: BlogApiService, 
    //   useClass: BlogApiService
    // },
    // BlogApiService
  ]
})
export class BlogModule { }

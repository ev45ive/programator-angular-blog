import { Component, OnInit, Inject } from '@angular/core';
import { BlogApiService } from '../../services/blog-api.service';
import { Post } from '../../models/post';

@Component({
  selector: 'app-search-posts',
  templateUrl: './search-posts.component.html',
  styleUrls: ['./search-posts.component.scss']
})
export class SearchPostsComponent implements OnInit {
  searchResults: Post[]
  message: string

  constructor(protected service: BlogApiService) { }

  searchPosts(q: string) {
    this.service.searchPosts(q).subscribe({
      next: (data) => this.searchResults = data,
      error: (error) => this.message = (error.message),
    })
  }

  ngOnInit(): void { }

}

import { Component, OnInit, Inject } from '@angular/core';
import { BlogApiService } from '../../services/blog-api.service';
import { Post } from '../../models/post';

@Component({
  selector: 'app-recent-posts',
  templateUrl: './recent-posts.component.html',
  styleUrls: ['./recent-posts.component.scss']
})
export class RecentPostsComponent implements OnInit {

  recentPosts: Post[] = [
    {
      "id": 100,
      "title": "To nie jest post z serwera!",
      "subtitle": "Problems look mighty small from 150 miles up",
      "userId": 1,
      "date": "2019-09-23T22:00:00.000Z",
      "body": ""
    }, {
      "id": 101,
      "title": "Ten też nie...",
      "subtitle": "Problems look mighty small from 150 miles up",
      "userId": 1,
      "date": "2019-09-23T22:00:00.000Z",
      "body": ""
    }
  ]
  
  message: string;

  constructor(protected service: BlogApiService) { }

  ngOnInit() {

    this.service.getRecent().subscribe({
      next: (data) => this.recentPosts = data,
      error: (error) => this.message = (error.message),
    })

  }

}

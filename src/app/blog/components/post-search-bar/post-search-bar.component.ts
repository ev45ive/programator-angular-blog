import { Component, OnInit,  Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-post-search-bar',
  templateUrl: './post-search-bar.component.html',
  styleUrls: ['./post-search-bar.component.scss']
})
export class PostSearchBarComponent implements OnInit {

  // a-output-event 
  @Output() searchChange = new EventEmitter<string>();

  constructor() { }

  search(query: string) {
    this.searchChange.emit(query)
  }

  ngOnInit(): void {
  }

}

https://blackrockdigital.github.io/startbootstrap-clean-blog/#

JSON.stringify( $$('.post-preview').map( p => {
    return {
        title: $('.post-title',p).text().trim(),
        subtitle: $('.post-subtitle',p).text().trim(),
        userId: 1,
        date: new Date($('.post-meta',p).text().match(/on (.*)/).pop()).toJSON(),
        body:``
    }
}))